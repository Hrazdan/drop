from django.contrib import admin
from .models import BoardMember

# Register your models here.

admin.site.register(BoardMember)

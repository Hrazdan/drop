from django.db import models
from login.models import DropAbstractUser

# Create your models here.

class BoardMember(DropAbstractUser):
    last_name = models.CharField('Last name', max_length=30, blank=True)
    photo = models.ImageField(verbose_name='Photo', upload_to='/static/', blank=True)
    position = models.TextField('Position', blank=True)

    class Meta:
        db_table = 'BoardMember'
        verbose_name = 'Board member'
        verbose_name_plural = 'Board members'
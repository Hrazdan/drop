from django.db import models
from login.models import DropAbstractUser

# Create your models here.

class Entrepreneur(DropAbstractUser):
    class Meta:
        db_table = 'Entrepreneur'

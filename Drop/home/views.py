from django.shortcuts import render, render_to_response
from boardmember.models import BoardMember
from projects.models import Project
from partner.models import Partner
import datetime

# Create your views here.

def main_page(request):
    context = {}
    print ('aaa')
    # current projects
    projects = Project.objects.filter(deadlines__gte=datetime.date.today())
    context['projects'] = projects
    # board members
    board_members = BoardMember.objects.all()
    context['board_members'] = board_members
    # partners
    partners = Partner.objects.all()
    context['partners'] = partners
    return render_to_response('main_page.html', context)

def how_it_works(requset):
	 return render_to_response('how_it_ works .html')

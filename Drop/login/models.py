from django.db import models

# Create your models here.

class DropAbstractUser(models.Model):
    name = models.CharField('First name', max_length=15, blank=True)
    email = models.EmailField('Email address')
    description = models.TextField('Description', blank=True)
    site = models.URLField('Site', blank=True)
    facebook = models.URLField('Facebook', blank=True)
    linkedin = models.URLField('Linkedin', blank=True)

    class Meta:
        abstract = True


class DropUser(DropAbstractUser):
    class Meta:
        db_table = 'DropUser'
        abstract = False

from django.db import models
from projects.models import Project
# Create your models here.

class News(models.Model):
    title = models.CharField('Title', max_length=100)
    text = models.TextField(verbose_name='Text')
    project = models.ForeignKey(Project, null=True)
    video_link = models.URLField('Video link', blank=True)
    add_date = models.DateField(auto_now=True)

    class Meta:
        db_table = 'News'
        verbose_name_plural = 'News'
from django.shortcuts import render_to_response
from django.http import Http404
from projects.models import *
from .models import News

# Create your views here.

def get_news(request, project_id):
    context = {}
    try:
        project_id = int(project_id)
        project = Project.objects.get(pk=project_id)
    except:
        raise Http404
    context['news'] = project.news_set.all()
    return render_to_response('news.html', context)
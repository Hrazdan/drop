from django.db import models
from login.models import DropAbstractUser

# Create your models here.

class Partner(DropAbstractUser):
    logo = models.ImageField(verbose_name='Logo', upload_to='/static/', blank=True)
    area = models.CharField('Main activities area', max_length=50, blank=True)
    phone = models.CharField('Phone', max_length=15, blank=True)
    address = models.CharField('Address', max_length=80, blank=True)

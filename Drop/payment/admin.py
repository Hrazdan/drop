from django.contrib import admin
from .models import PaymentSystem

# Register your models here.
admin.site.register(PaymentSystem)
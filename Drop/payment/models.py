from django.db import models
from login.models import DropUser

# Create your models here.

class PaymentSystem(models.Model):
    name = models.CharField(max_length=20, verbose_name='System name')
    logo = models.ImageField(upload_to='/static/', blank=True, null=True, verbose_name='Logo')
    class Meta:
        db_table = 'PaymentSystem'
        verbose_name = 'Payment system'
        verbose_name_plural = 'Payment systems'
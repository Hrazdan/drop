from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(ProjectCategory)
admin.site.register(Project)
admin.site.register(ProjectPayment)
admin.site.register(ProjectUpdate)
admin.site.register(ProjectComment)
admin.site.register(ProjectUpdateComment)
admin.site.register(ProjectDocument)


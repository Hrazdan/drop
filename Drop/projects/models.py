from django.db import models
from login.models import DropUser
from entrepreneur.models import Entrepreneur
from login.models import DropUser
from payment.models import PaymentSystem


class ProjectCategory(models.Model):
    name = models.CharField('Project category', max_length=50)
    description = models.TextField('Description', blank=True)

    class Meta:
        db_table = 'ProjectCategory'
        verbose_name = 'Project category'
        verbose_name_plural = 'Project categories'


class Project(models.Model):
    title = models.CharField('Title', max_length=100)
    main_photo = models.ImageField('Main photo', upload_to='/static/', blank=True, null=True)
    cover_photo = models.ImageField('Cover photo', upload_to='/static/', blank=True, null=True)
    sum = models.DecimalField('Sum', max_digits=10, decimal_places=2, default=0)
    backers = models.ManyToManyField(DropUser, through='ProjectPayment', verbose_name='Users')
    deadlines = models.DateField(verbose_name='Deadlines')
    entrepreneur = models.ForeignKey(Entrepreneur, verbose_name='Entrepreneur')
    description = models.TextField('Description', blank=True)
    category = models.ForeignKey(ProjectCategory, verbose_name='Category')
    website = models.URLField('Website', blank=True)

    class Meta:
        db_table = 'Project'
        ordering = ['title']
        verbose_name = 'Project'
        verbose_name_plural = 'Projects'


class ProjectPayment(models.Model):
    project = models.ForeignKey(Project, verbose_name='Project')
    user = models.ForeignKey(DropUser, verbose_name='User')
    p_system = models.ForeignKey(PaymentSystem, verbose_name='Payment system')
    amount = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Amount')
    payment_date = models.DateField(auto_now=True, verbose_name='Payment date')

    class Meta:
        db_table = 'ProjectPayment'
        verbose_name = 'Project payment'
        verbose_name_plural = 'Project payments'


class AbstractComment(models.Model):
    add_date = models.DateField(auto_now=True)
    text = models.TextField('Comment')
    class Meta:
        abstract = True


class ProjectUpdate(AbstractComment):
    project = models.ForeignKey(Project)

    class Meta:
        db_table = 'ProjectUpdate'
        verbose_name = 'Project update'
        verbose_name_plural = 'Project updates'


class ProjectComment(AbstractComment):
    project = models.ForeignKey(Project)
    user = models.ForeignKey(DropUser)

    class Meta:
        db_table = 'ProjectComment'
        verbose_name = 'Project comment'
        verbose_name_plural = 'Project comments'

class ProjectUpdateComment(AbstractComment):
    user = models.ForeignKey(DropUser)
    update = models.ForeignKey(ProjectUpdate)

    class Meta:
        db_table = 'ProjectUpdateComment'
        verbose_name = 'Project update comment'
        verbose_name_plural = 'Project update comments'


class ProjectDocument(models.Model):
    project = models.ForeignKey(Project)
    description = models.TextField('Description', blank=True)
    document = models.FileField(upload_to='/static/', blank=True, null=True)

    class Meta:
        db_table = 'ProjectDocument'
        verbose_name = 'Project document'
        verbose_name_plural = 'Project documents'

from django.shortcuts import render, render_to_response
from django.http import Http404
from django.db.models import Sum
from .models import Project
from payment.models import Payment

# Create your views here.

def project_page(request, project_id):

    context = {}
    try:
        id = int(project_id)
    except ValueError:
        raise Http404
    try:
        project = Project.objects.get(pk=id)
    except Project.DoesNotExist:
        raise Http404

    context['project'] = project
    context['entrepreneur'] = project.entrepreneur

    payments = []
    for total in range(1,6):
        payments.append(project.payment_set.filter(amount=total).count())

    context['payments'] = payments
    context['raised_sum'] = project.payment_set.all().aggregate(raised_sum=Sum('amount'))['raised_sum']
    context['needed_sum'] = project.sum - context['raised_sum']
    context['deadlines'] = project.deadlines
    context['updates'] = project.projectupdate_set.all()
    context['documents'] = project.projectdocument_set.all()
    context['comments'] = project.projectcomment_set.all().annotate(user='user__name', avatar='user__photo')

    return render_to_response('project.html', context)